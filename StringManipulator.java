import java.util.Scanner;
import java.util.*;


public class StringManipulator {

   public static String[] changingStr;
   
   public static void main(String[] args){
      boolean active = true;
      System.out.println("Please enter the string you would like to manipulate");
      Scanner userInput = new Scanner(System.in);
      String changingStr = userInput.nextLine();
      System.out.println("");
      System.out.println(changingStr);
      System.out.println(changingStr.length());
      String[] changingArray = convertToStringArr(changingStr);
      //System.out.println(Arrays.toString(changingArray));
      System.out.println("What would you like to do to the string?");
      System.out.println("Enter either: reverse, replace all, remove or quit");
      while (active) {
        // System.out.println("What would you like to do to the string?");
        // System.out.println("Enter either: reverse, replace all, remove or quit");
         Scanner actionScanner = new Scanner(System.in);
         String userAction = actionScanner.nextLine();
         System.out.println(userAction);
         String formattedUserAction = userAction.toLowerCase();
         if (formattedUserAction.equals("reverse")){
            changingArray = reverse(changingArray);
            System.out.println("The reversed string is: " + printString(changingArray));
            System.out.println("Please input another action");
         } else if (formattedUserAction.equals("replace all")){
            changingArray = replaceAll(changingArray);
            System.out.println("The new string is: " + printString(changingArray));
            System.out.println("Please input another action");

         } else if (formattedUserAction.equals("remove")){
            changingArray = remove(changingArray);
            System.out.println("The new string is: " + printString(changingArray));
            System.out.println("Please input another action");

         } else if (formattedUserAction.equals("quit")){
            active = false;
         } else {
            System.out.println("Please input an action");
         }
      }

   }
   
   // takes input and converts to string array so user can edit
   // the string using inputs
   public static String[] convertToStringArr(String str) {
      String arrOfInput[] = str.split("");
      return arrOfInput;
   }
   
   // reverses the string 
   public static String[] reverse(String[] str) {
     String[] reversed = new String[str.length];
     for (int i=0; i<str.length; i++) {
         reversed[i] = str[str.length-1-i];
     }
     return reversed;
   }
   // user selects what character they would like to remove
   // method removes that character
   public static String[] replaceAll(String[] str) {
      // get char to remove
      System.out.println("Which character would you like to remove from the string?");
      
      boolean replaced = false;
      
      while (replaced == false) {
         boolean isPresent = false;
         Scanner firstSc = new Scanner(System.in);
         String charToRemove = firstSc.nextLine();
         System.out.println(charToRemove);
         for (String s: str){
            
            if(s.equals(charToRemove)){
               isPresent = true;
               break;
            }       
         }  
         // conditional to handle the case when the input the user wants to replace
         // does not exist in the string
         if (isPresent == true) {
            System.out.println("Which character would you like to replace " + charToRemove + "?");
            Scanner secondSc = new Scanner(System.in);
            String charToPlace = secondSc.nextLine();
            for (int i=0; i<str.length; i++) {
               if (str[i].equals(charToRemove)){
                  str[i]=charToPlace;
               }
            }
            replaced = true;
                  
            
         } else {
               System.out.println("The character you wish to remove is not in this string. Please try again.");
         } 
      }

      return str;
   }
   
   // Removes a user-specified character
   // creates string array that contains a set amount of places
   // then that array is repalced with the correct dimension
   // int removals is a value that helps find the correct length of the array to return
   // when the for loop finds a character that is to be removed, it iterates the removal variable
   public static String[] remove(String[] str) {
      boolean isPresent = false;
      boolean removed = false;
      int howManyMissing;
      int removals = 0;
      System.out.println("Which character would you like to remove?");
      String[] copy = new String[10];
      while (!removed) {
         howManyMissing = 0;
         Scanner thirdSc = new Scanner(System.in);
         String charToRemove = thirdSc.nextLine();
         for (String s : str) {
            if(s.equals(charToRemove)){
               isPresent = true;
               removals++;
               removed = true;
            } else {
               howManyMissing++;
            }
            if (howManyMissing == str.length) {
               System.out.println("Sorry, the chracter you wish to remove is not in the string. Please input another value.");
            }
         }
         
         copy = new String[str.length-removals];
         
         int newIndex = 0;
         if (isPresent == true) {
            for (int i = 0; i<str.length; i++) {
               if (!str[i].equals(charToRemove)){
                  copy[newIndex] = str[i];
                  newIndex++;
                  
               } 
            }
         System.out.println(Arrays.toString(copy));
        
         }
   
       
         
       }
      return copy;
   }
   
   // for printing the string when modified by main
   public static String printString (String[] strToPrint) {
      String sentence = "";
      for (int i=0; i<strToPrint.length; i++) {
         sentence = sentence + strToPrint[i];
         
      }
      return sentence;
   }
}
      
